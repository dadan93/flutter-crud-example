import 'package:dio/dio.dart';
import 'package:flutter_test_crud_example/models/detail_user_model.dart';

import '../models/add_user_model.dart';
import '../models/user_model.dart';
import '../utils/constant.dart';

class UserViewModel {
  final Dio _dio = Dio();

  Future<List<UserModel>?> userData(int page, int perPage) async {
    final url = "$baseUrl/public/v2/users?page=$page&per_page=$perPage";
    try {
      Response res = await _dio.get(
        url,
      );
      print(res.data);
      if (res.statusCode == 200) {
        final List<UserModel> results =
            List<UserModel>.from(res.data.map((e) => UserModel.fromJson(e)));
        return results;
      }
      throw res.statusCode!;
    } catch (err) {
      print(err);
      return null;
    }
  }

  Future<DetailUserModel?> userDetail(int userId) async {
    print(userId);
    final url = "$baseUrl/public/v2/users/$userId";
    try {
      Response res = await _dio.get(
        url,
      );
      print(res.data);
      if (res.statusCode == 200) {
        final DetailUserModel results = DetailUserModel.fromJson(res.data);
        return results;
      }
      throw res.statusCode!;
    } catch (err) {
      print(err);
      return null;
    }
  }

  Future<AddUserModel?> addUser(
      String name, String email, String gender, String status) async {
    const url = "$baseUrl/public/v2/users";
    try {
      Response res = await _dio.post(url,
          data: {
            "name": name,
            "email": email,
            "gender": gender,
            "status": status
          },
          options: Options(headers: {"Authorization": "Bearer $token"}));
      print(res.data);
      final AddUserModel results = AddUserModel.fromJson(res.data);
      return results;
    } on DioError catch (err) {
      throw err.response!.data;
    }
  }

  Future<AddUserModel?> editUser(
      int id, String name, String email, String gender, String status) async {
    final url = "$baseUrl/public/v2/users/$id";
    try {
      Response res = await _dio.put(url,
          data: {
            "name": name,
            "email": email,
            "gender": gender,
            "status": status
          },
          options: Options(headers: {"Authorization": "Bearer $token"}));
      print(res.data);
      final AddUserModel results = AddUserModel.fromJson(res.data);
      return results;
    } on DioError catch (err) {
      throw err.response!.data;
    }
  }

  Future<String> deleteUser(int id) async {
    final url = "$baseUrl/public/v2/users/$id";
    try {
      Response res = await _dio.delete(url,
          options: Options(headers: {"Authorization": "Bearer $token"}));
      print(res.data);

      return res.data;
    } on DioError catch (err) {
      throw err.response!.data;
    }
  }
}
