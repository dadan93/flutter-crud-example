import 'package:flutter/material.dart';

class ColorPalette {
  static const activeColor = Color(0xFFFFBF00);
  static const themeColor = Color(0xFFFFBF00);
  static const cancelColor = Color(0xFFcf0f0f);
  static const deleteColor = Color(0xFFcf0f0f);
  static const disableColor = Color(0xFFDFDEDE);
  static const hintColor = Color(0xFF999999);
  static const blackColor = Color(0xFF393939);
  static const greyColor = Color(0xFF999999);
  static const orangeGradient1 = Color(0xFFcf0f0f);
  static const orangeGradient2 = Color(0xFFcf0f0f);
  static const colorBodyText = Color(0xFF5D5F61);
  static const colorHintText = Color(0xFF989B9D);
  static const colorFilledTextField = Color(0xFFF4F5F6);
  static const colorBorderCard = Color(0xFFE4EAF0);
}
