import 'package:flutter/material.dart';
import 'package:flutter_test_crud_example/providers/users_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import '../utils/color_palette.dart';
import 'widgets/input_decoration.dart';

class AddUserPage extends StatefulWidget {
  const AddUserPage({super.key});

  @override
  State<AddUserPage> createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _genderController = TextEditingController();
  final TextEditingController _statusController = TextEditingController();
  final List<Map<String, Object>> _status = [
    {
      "id": 1,
      "status": "active",
    },
    {
      "id": 2,
      "status": "inactive",
    },
  ];
  final List<Map<String, Object>> _gender = [
    {
      "id": 1,
      "gender": "male",
    },
    {
      "id": 2,
      "gender": "female",
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Detail User"),
        elevation: 0.0,
        backgroundColor: ColorPalette.themeColor,
      ),
      body: Consumer<UsersProvider>(
        builder: (context, value, child) => Container(
          margin: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                      controller: _nameController,
                      decoration: inputDecoration("Nama")),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                      controller: _emailController,
                      decoration: inputDecoration("Email")),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                      controller: _genderController,
                      readOnly: true,
                      onTap: () {
                        _selectGender(context);
                      },
                      decoration: inputDecorationWithIcon("Gender")),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                      controller: _statusController,
                      readOnly: true,
                      onTap: () {
                        _selectStatus(context);
                      },
                      decoration: inputDecorationWithIcon("Status")),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: ColorPalette.themeColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          onPressed: () {
                            value
                                .addUser(
                                    _nameController.text,
                                    _emailController.text,
                                    _genderController.text,
                                    _statusController.text)
                                .then((value) {
                              if (value != null) {
                                Future.delayed(const Duration(seconds: 1))
                                    .then((value) {
                                  return Fluttertoast.showToast(
                                      msg: "Data Berhasil Disimpan",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.amber,
                                      textColor: Colors.white);
                                });
                                Navigator.pop(context);
                              } else {
                                Future.delayed(const Duration(seconds: 1))
                                    .then((value) {
                                  return Fluttertoast.showToast(
                                      msg: "Data Gagal Disimpan",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white);
                                });
                              }
                            });
                          },
                          child: Text(
                            "BUAT PENGGUNA",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(color: Colors.white),
                          ))),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectGender(BuildContext context) {
    showMaterialModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height * 0.20,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: ListView.builder(
          itemCount: _gender.length,
          itemBuilder: (context, index) => GestureDetector(
            onTap: () {
              setState(() {
                _genderController.text = _gender[index]["gender"].toString();
              });
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Text(_gender[index]["gender"].toString()),
                  const Divider(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _selectStatus(BuildContext context) {
    showMaterialModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height * 0.20,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: ListView.builder(
          itemCount: _status.length,
          itemBuilder: (context, index) => GestureDetector(
            onTap: () {
              setState(() {
                _statusController.text = _status[index]["status"].toString();
              });
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Text(_status[index]["status"].toString()),
                  const Divider(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
