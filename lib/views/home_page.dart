import 'package:flutter/material.dart';
import 'package:flutter_test_crud_example/providers/users_provider.dart';
import 'package:provider/provider.dart';

import '../utils/color_palette.dart';
import '../views/detail_user_page.dart';
import '../views/widgets/input_decoration.dart';

import 'add_user_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = ScrollController();
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<UsersProvider>(context, listen: false).fetchData();
    });
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.offset) {
        Provider.of<UsersProvider>(context, listen: false).fetchScrollData();
      }
    });
    super.initState();
  }

  refreshData() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<UsersProvider>(context, listen: false).fetchData();
    });
    setState(() {});
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController searchController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorPalette.themeColor,
        elevation: 0,
        title: TextFormField(
            controller: searchController,
            decoration: inputDecoration("SEARCH")),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
            Navigator.push(context, MaterialPageRoute(builder: (context) {
          return const AddUserPage();
        })).then((value) {
          refreshData();
        }),
        backgroundColor: ColorPalette.themeColor,
        child: const Icon(Icons.add),
      ),
      body: Consumer<UsersProvider>(
        builder: (context, value, child) {
          if (value.isLoading == true) {
            return const Center(child: CircularProgressIndicator());
          } else {
            final usersData = value.userModel;
            return Container(
              margin: const EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
              child: ListView.builder(
                controller: controller,
                itemCount: usersData.length + 1,
                itemBuilder: (context, index) {
                  if (index < usersData.length) {
                    return GestureDetector(
                      onTap: () {
                        value.fetchUserDetail(usersData[index].id!);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return const DetailUserPage();
                        })).then((value) {
                          refreshData();
                        });
                      },
                      child: Container(
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(bottom: 16.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.black)),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(usersData[index].id!.toString()),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(usersData[index].name!),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(usersData[index].email!),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(usersData[index].gender!),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(usersData[index].status!),
                            ],
                          ),
                        ),
                      ),
                    );
                  } else {
                    return value.hasMore
                        ? const Center(child: CircularProgressIndicator())
                        : const Center(child: Text("Tidak ada data"));
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}
