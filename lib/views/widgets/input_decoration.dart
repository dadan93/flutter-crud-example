import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../utils/color_palette.dart';

InputDecoration inputDecoration(String hintText) => InputDecoration(
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      hintText: hintText,
      hintStyle: GoogleFonts.poppins(
        color: ColorPalette.colorHintText,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      errorStyle: GoogleFonts.poppins(
        color: Colors.red,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: ColorPalette.greyColor),
        borderRadius: BorderRadius.circular(5.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: ColorPalette.activeColor)),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5.0),
      ),
    );

InputDecoration inputDecorationWithIcon(String hintText) => InputDecoration(
      suffixIcon: const Icon(Icons.keyboard_arrow_down),
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      hintText: hintText,
      hintStyle: GoogleFonts.poppins(
        color: ColorPalette.colorHintText,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      errorStyle: GoogleFonts.poppins(
        color: Colors.red,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: ColorPalette.greyColor),
        borderRadius: BorderRadius.circular(5.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: ColorPalette.activeColor)),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5.0),
      ),
    );
