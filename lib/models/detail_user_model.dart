// To parse this JSON data, do
//
//     final detailUserModel = detailUserModelFromJson(jsonString);

import 'dart:convert';

DetailUserModel detailUserModelFromJson(String str) =>
    DetailUserModel.fromJson(json.decode(str));

String detailUserModelToJson(DetailUserModel data) =>
    json.encode(data.toJson());

class DetailUserModel {
  DetailUserModel({
    this.id,
    this.name,
    this.email,
    this.gender,
    this.status,
  });

  int? id;
  String? name;
  String? email;
  String? gender;
  String? status;

  factory DetailUserModel.fromJson(Map<String, dynamic> json) =>
      DetailUserModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        gender: json["gender"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "gender": gender,
        "status": status,
      };
}
