import 'package:flutter/material.dart';
import 'package:flutter_test_crud_example/models/add_user_model.dart';
import 'package:flutter_test_crud_example/models/detail_user_model.dart';
import '../models/user_model.dart';
import '../viewmodels/user_vm.dart';

class UsersProvider extends ChangeNotifier {
  final UserViewModel _userVm = UserViewModel();
  List<UserModel>? _userModel;
  List<UserModel> get userModel => _userModel!;
  DetailUserModel? _userDetail;
  DetailUserModel get userDetail => _userDetail!;
  final TextEditingController _statusController = TextEditingController();
  TextEditingController get statusController => _statusController;
  final TextEditingController _genderController = TextEditingController();
  TextEditingController get genderController => _genderController;

  bool isLoading = false;
  bool isScrollLoading = false;
  int page = 1;
  int perPage = 10;
  bool hasMore = true;

  Future<List<UserModel>?> fetchData() async {
    isLoading = true;
    notifyListeners();
    final res = await _userVm.userData(page, perPage);
    _userModel = res!;
    isLoading = false;
    notifyListeners();
    return _userModel;
  }

  Future<List<UserModel>?> fetchScrollData() async {
    isScrollLoading = true;
    perPage += 10;
    notifyListeners();
    final res = await _userVm.userData(page, perPage);
    _userModel = res!;
    isScrollLoading = false;
    if (res.length < perPage) {
      hasMore = false;
      notifyListeners();
    }
    isLoading = false;
    notifyListeners();
    return _userModel;
  }

  editGender(String gender) {
    _genderController.text = gender;
    notifyListeners();
  }

  editStatus(String status) {
    _statusController.text = status;
    notifyListeners();
  }

  Future<DetailUserModel?> fetchUserDetail(int id) async {
    isLoading = true;
    notifyListeners();
    final res = await _userVm.userDetail(id);
    _userDetail = res!;
    _genderController.text = res.gender!;
    _statusController.text = res.status!;
    isLoading = false;
    notifyListeners();
    return _userDetail;
  }

  Future<AddUserModel?> addUser(
      String name, String email, String gender, String status) async {
    final res = await _userVm.addUser(name, email, gender, status);
    return res;
  }

  Future<AddUserModel?> editUser(
      int id, String name, String email, String gender, String status) async {
    final res = await _userVm.editUser(id, name, email, gender, status);
    return res;
  }

  Future<void> deleteUser(int id) async {
    final res = await _userVm.deleteUser(id);
  }
}
